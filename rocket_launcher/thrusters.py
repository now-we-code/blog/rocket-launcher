import asyncio
from .exceptions import FuelTankEmptyError


class Thrusters:
    thrusters_activated = False
    base_time = 0.2
    fuel_per_base_time = 1

    def __init__(self, fuel_tank):
        self.fuel_tank = fuel_tank

    def use_thrusters(self):
        self.thrusters_activated = True

        async def _use_thrusters():
            while self.thrusters_activated:
                try:
                    self.fuel_tank.use_fuel(self.fuel_per_base_time)
                except FuelTankEmptyError:
                    self.stop_thrusters()

                await asyncio.sleep(self.base_time)

        asyncio.create_task(_use_thrusters())

    def stop_thrusters(self):
        self.thrusters_activated = False
