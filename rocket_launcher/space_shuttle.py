import asyncio

from .fuel_tank import FuelTank
from .thrusters import Thrusters


class SpaceShuttle:

    def __init__(self):
        self.fuel_tank = FuelTank()
        self.thrusters = Thrusters(self.fuel_tank)

    def launch(self):
        self.thrusters.use_thrusters()
