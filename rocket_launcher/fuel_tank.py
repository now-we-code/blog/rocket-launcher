import asyncio

from .exceptions import FuelTankEmptyError, FuelTankFullError, NotPositiveError


class FuelTank:
    fuel_level = 0
    max_level = 100
    is_filling = False
    base_time = 0.2
    fuel_per_base_time = 5

    def get_fuel_level(self):
        return self.fuel_level

    def load_fuel(self, quantity):
        if quantity < 0:
            raise NotPositiveError

        if self.fuel_level + quantity > self.max_level:
            self.fuel_level = self.max_level
            raise FuelTankFullError
        else:
            self.fuel_level += quantity

    def use_fuel(self, quantity):
        if quantity < 0:
            raise NotPositiveError

        if self.fuel_level - quantity < 0:
            self.fuel_level = 0
            raise FuelTankEmptyError

        self.fuel_level -= quantity

    def fill(self):
        self.is_filling = True

        async def _fill():
            while self.is_filling:
                try:
                    self.load_fuel(self.fuel_per_base_time)
                except FuelTankFullError:
                    self.stop_fill()

                await asyncio.sleep(self.base_time)

        asyncio.create_task(_fill())

    def stop_fill(self):
        self.is_filling = False
