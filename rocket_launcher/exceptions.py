class FuelTankEmptyError(Exception):
    pass


class FuelTankFullError(Exception):
    pass


class NotPositiveError(Exception):
    pass
