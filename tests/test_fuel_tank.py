import asyncio

import pytest
from rocket_launcher.fuel_tank import FuelTank
from rocket_launcher.exceptions import FuelTankEmptyError, FuelTankFullError, NotPositiveError


@pytest.fixture(scope="module")
def resource():
    print("setup")
    yield "resource"
    print("teardown")


class TestFuelTank:
    def test_fuel_tank_initialization(self):
        # arrange

        # act
        fuel_tank = FuelTank()

        # assert
        assert fuel_tank.get_fuel_level() == 0

    def test_load_fuel_should_add_fuel(self):
        # arrange
        test_quantity = 50
        fuel_tank = FuelTank()

        # act
        fuel_tank.load_fuel(test_quantity)

        # assert
        assert fuel_tank.get_fuel_level() == test_quantity

    def test_load_fuel_should_raise_error_when_input_negative(self):
        # arrange
        fuel_tank = FuelTank()

        # act & assert
        with pytest.raises(NotPositiveError):
            fuel_tank.load_fuel(-50)

    def test_load_fuel_should_raise_error_when_max_level_exeeded(self):
        # arrange
        fuel_tank = FuelTank()

        # act & assert
        with pytest.raises(FuelTankFullError):
            fuel_tank.load_fuel(101)

    def test_use_fuel_should_substract_quantity(self):
        # arrange
        fuel_tank = FuelTank()
        fuel_tank.load_fuel(50)

        # act
        fuel_tank.use_fuel(20)

        # assert
        assert fuel_tank.get_fuel_level() == 30

    def test_use_fuel_should_raise_error_when_input_negative(self):
        # arrange
        fuel_tank = FuelTank()

        # act & assert
        with pytest.raises(NotPositiveError):
            fuel_tank.use_fuel(-50)

    def test_use_fuel_should_raise_error_when_fuel_empty(self):
        # arrange
        fuel_tank = FuelTank()

        # act & assert
        with pytest.raises(FuelTankEmptyError):
            fuel_tank.use_fuel(20)

    @pytest.mark.asyncio
    async def test_fill_shoud_fill_25_fuel_units_in_1_sec(self):
        # arrange
        fuel_tank = FuelTank()

        # act
        fuel_tank.fill()

        await asyncio.sleep(1)
        fuel_tank.stop_fill()

        # assert
        assert fuel_tank.get_fuel_level() == 25

    @pytest.mark.asyncio
    async def test_fill_shoud_stop_when_tank_full(self):
        # arrange
        fuel_tank = FuelTank()

        # act
        fuel_tank.fill()

        await asyncio.sleep(5)

        assert fuel_tank.get_fuel_level() == fuel_tank.max_level
