import asyncio

from rocket_launcher.space_shuttle import SpaceShuttle


async def main():
    space_shuttle = SpaceShuttle()
    space_shuttle.fuel_tank.fill()

    await asyncio.sleep(2)
    space_shuttle.fuel_tank.stop_fill()
    print('stop Fill')
    print(space_shuttle.fuel_tank.get_fuel_level())

    space_shuttle.launch()

    await asyncio.sleep(1)
    print(space_shuttle.fuel_tank.get_fuel_level())
    await asyncio.sleep(1)
    print(space_shuttle.fuel_tank.get_fuel_level())
    await asyncio.sleep(1)
    print(space_shuttle.fuel_tank.get_fuel_level())
    # space_shuttle.fuel_tank.stop_fill()

if __name__ == "__main__":
    asyncio.run(main())
